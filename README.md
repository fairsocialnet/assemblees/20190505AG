# Assemblée Générale 2019
## Introduction/bienvenue du président
* Rafael MUNOZ

Rapport d'activités, présentation (lien)


Vote 1 - Les scrutateurs: JPM+?

Départ de la trésocrière:  ?
Avec nos remerciements

vote 2 - Approbation des comptes
* compte-tenu des débuts d'activités et le nombre très limité des écritures, la totalité des comptes est exposé à l'assemblée.

- Les comptes sont approuvés et décharge au comité sont votés à l'unanimité

L'appel à nouveau membre pour rejoindre le comité, n'a pas permis de renouveler son 3ème membre. Nous devrons renouveler d'effort en 2019 pour étendre nos membres et motiver à rejoindre le comité.

Vote 3 - Renouvellement du comité: Rafael MUNOZ, Pascal Kotté
- élus à l'unanimité.

Le nouveau comité est réduit à 2 membres. 
* Rafael Munoz reste le président.
* Pascal Kotté devient le trésorier.

Le rôle de secrétaire est repris par Rafaël.
